import gamnit::textures
import gamnit::tileset

class DrawingImages

	var tiles_texture = new Texture("level/tiles_sheet.png")
	var tiles = new TileSet(tiles_texture, 17, 17)

	#UI textures
	var night_maroon = new Texture("ui/night_maroon.png")
	var night_blue = new Texture("ui/night_blue.png")
	var hearts = new Texture("ui/hearts.png")
	var full_heart: Texture = hearts.subtexture(0, 0, 36, 32)
	var half_heart: Texture = hearts.subtexture(38, 0, 36, 32)
	var empty_heart: Texture = hearts.subtexture(76, 0, 36, 32)

	#void tile
	var void: Texture = tiles[0,5] is lazy

	#grass tiles
	var grass1: Texture = tiles[5,0] is lazy
	var grass2: Texture = tiles[5,1] is lazy
	var grasses: Array[Texture] = [grass1, grass2] is lazy

	#dirt tiles
	var dirt1: Texture = tiles[8,10] is lazy
	var dirt2: Texture = tiles[6,0] is lazy
	var dirt3: Texture = tiles[6,1] is lazy
	var dirt_up_right: Texture = tiles[9,9] is lazy
	var dirt_up_left: Texture = tiles[7,9] is lazy
	var dirt_up: Texture = tiles[8,9] is lazy
	var dirt_right: Texture = tiles[9,10] is lazy
	var dirt_left: Texture = tiles[7,10] is lazy
	var dirt_bottom: Texture = tiles[8,11] is lazy
	var dirt_bottom_left: Texture = tiles[7,11] is lazy
	var dirt_bottom_right: Texture = tiles[9,11] is lazy
	var dirt: Texture = new Texture("level/dirt.png")
	var dirt_horizontal: Texture = new Texture("level/dirt_horizontal.png")
	var dirt_vertical: Texture = new Texture("level/dirt_vertical.png")
	var dirt_cross_left: Texture = new Texture("level/dirt_cross_left.png")
	var dirt_cross_right: Texture = new Texture("level/dirt_cross_right.png")
	var dirt_cross_up: Texture = new Texture("level/dirt_cross_up.png")
	var dirt_cross_down: Texture = new Texture("level/dirt_cross_down.png")
	var dirt_cross: Texture = new Texture("level/dirt_cross.png")
	var dirt_end_up: Texture = new Texture("level/dirt_end_up.png")
	var dirt_end_down: Texture = new Texture("level/dirt_end_down.png")
	var dirt_end_right: Texture = new Texture("level/dirt_end_right.png")
	var dirt_end_left: Texture = new Texture("level/dirt_end_left.png")
	var dirt_left_down: Texture = tiles[8,7] is lazy
	var dirt_left_up: Texture = tiles[8,8] is lazy
	var dirt_right_down: Texture = tiles[7,7] is lazy
	var dirt_right_up: Texture = tiles[7,8] is lazy

	var dungeon_wall: Texture = tiles[22, 16] is lazy
	var dungeon_floor: Texture = tiles[26, 18] is lazy
	var dungeon_pillar: Texture = tiles[21, 20] is lazy
	var dungeon_stairs: Texture = tiles[22, 18] is lazy
	var dungeon_gate: Texture = tiles[33, 1] is lazy

	var treasure: Texture = tiles[43, 11] is lazy

	#props tiles
	var green_bush: Texture = new Texture("level/green_bush.png")

	#water tiles
	var water1: Texture = tiles[0,0] is lazy
	var water2: Texture = tiles[1,0] is lazy
	var water_grass_up: Texture = tiles[3,0] is lazy
	var water_grass_up_right: Texture = tiles[4,0] is lazy
	var water_grass_up_left: Texture = tiles[2,0] is lazy
	var water_grass_right: Texture = tiles[4,1] is lazy
	var water_grass_left: Texture = tiles[2,1] is lazy
	var water_grass_bottom_left: Texture = tiles[2,2] is lazy
	var water_grass_bottom_right: Texture = tiles[4,2] is lazy
	var water_grass_bottom: Texture = tiles[3,2] is lazy

	#decoration tiles
	var nenuphar: Texture = tiles[25,11] is lazy
	var blue_flowers: Texture = tiles[3,13] is lazy
	var red_flowers: Texture = tiles[3,7] is lazy
	var white_flowers: Texture = tiles[3,10] is lazy
	var flowers: Array[Texture] = [blue_flowers, red_flowers, white_flowers] is lazy

	#trap tiles
	var trap_dirt_horizontal: Texture = new Texture("level/trap_dirt_horizontal.png")

	#player textures
	private var walk_sprites_left: Array[Int] = [0, 64, 128, 192, 256, 320, 384, 448, 512]
	private var slash_sprites_left: Array[Int] = [0, 64, 128, 192, 256, 320]

	var walk_back = new Texture("player/walk_back.png")
	var walk_left = new Texture("player/walk_left.png")
	var walk_front = new Texture("player/walk_front.png")
	var walk_right = new Texture("player/walk_right.png")
	var spellcast_back = new Texture("player/spellcast_back.png")
	var spellcast_left = new Texture("player/spellcast_left.png")
	var spellcast_front = new Texture("player/spellcast_front.png")
	var spellcast_right = new Texture("player/spellcast_right.png")

	var walk_back_sword = new Texture("player/walk_back_sword.png")
	var walk_left_sword = new Texture("player/walk_left_sword.png")
	var walk_front_sword = new Texture("player/walk_front_sword.png")
	var walk_right_sword = new Texture("player/walk_right_sword.png")
	var walk_back_crossbow = new Texture("player/walk_back_crossbow.png")
	var walk_left_crossbow = new Texture("player/walk_left_crossbow.png")
	var walk_front_crossbow = new Texture("player/walk_front_crossbow.png")
	var walk_right_crossbow = new Texture("player/walk_right_crossbow.png")
	var shoot_back_crossbow = new Texture("player/shoot_back_crossbow.png")
	var shoot_left_crossbow = new Texture("player/shoot_left_crossbow.png")
	var shoot_front_crossbow = new Texture("player/shoot_front_crossbow.png")
	var shoot_right_crossbow = new Texture("player/shoot_right_crossbow.png")
	var slash_back_sword = new Texture("player/slash_back_sword.png")
	var slash_left_sword = new Texture("player/slash_left_sword.png")
	var slash_front_sword = new Texture("player/slash_front_sword.png")
	var slash_right_sword = new Texture("player/slash_right_sword.png")

	var map_fists = new HashMap[Orientation, Array[Texture]] is lazy
	var map_fists2 = new HashMap[Orientation, Array[Texture]] is lazy
	var map_sword = new HashMap[Orientation, Array[Texture]] is lazy
	var map_sword2 = new HashMap[Orientation, Array[Texture]] is lazy
	var map_crossbow = new HashMap[Orientation, Array[Texture]] is lazy
	var map_crossbow2 = new HashMap[Orientation, Array[Texture]] is lazy

	var hurt = new Texture("player/hurt.png")
	var dead: Array[Texture] = [for i in [0..5] do hurt.subtexture(i*64, 5, 64, 58)]

	#weapon textures
	var sword_shiny: Texture = new Texture("props/sword_shiny.png")
	var crossbow: Texture = new Texture("props/crossbow.png")
	var arrow: Texture = new Texture ("props/arrow.png")

	#enemy textures
	var goblin_walk_front = new Texture("enemies/goblin_walk_front.png")
	var goblin_walk_back = new Texture("enemies/goblin_walk_back.png")
	var goblin_walk_left = new Texture("enemies/goblin_walk_left.png")
	var goblin_walk_right = new Texture("enemies/goblin_walk_right.png")
	var goblin_attack_front = new Texture("enemies/goblin_attack_front.png")
	var goblin_attack_back = new Texture("enemies/goblin_attack_back.png")
	var goblin_attack_left = new Texture("enemies/goblin_attack_left.png")
	var goblin_attack_right = new Texture("enemies/goblin_attack_right.png")
	var goblin_hurt = new Texture("enemies/goblin_dead.png")
	var goblin_dead: Array[Texture] = [for i in [0..4] do goblin_hurt.subtexture(i*64, 1, 64, 63)]
	var map_goblin = new HashMap[Orientation, Array[Texture]] is lazy
	var map_goblin2 = new HashMap[Orientation, Array[Texture]] is lazy
	var golem_walk_front = new Texture("enemies/golem_walk_front.png")
	var golem_walk_back = new Texture("enemies/golem_walk_back.png")
	var golem_walk_left = new Texture("enemies/golem_walk_left.png")
	var golem_walk_right = new Texture("enemies/golem_walk_right.png")
	var golem_attack_front = new Texture("enemies/golem_attack_front.png")
	var golem_attack_back = new Texture("enemies/golem_attack_back.png")
	var golem_attack_left = new Texture("enemies/golem_attack_left.png")
	var golem_attack_right = new Texture("enemies/golem_attack_right.png")
	var golem_hurt = new Texture("enemies/golem_dead.png")
	var golem_dead: Array[Texture] = [for i in [0..5] do golem_hurt.subtexture(i*64, 1, 64, 63)]
	var map_golem = new HashMap[Orientation, Array[Texture]] is lazy
	var map_golem2 = new HashMap[Orientation, Array[Texture]] is lazy

	init
	do
		map_fists = textures_to_map(walk_back, walk_front, walk_left, walk_right, 9, 9, 55)
		map_fists2 = textures_to_map(spellcast_back, spellcast_front, spellcast_left, spellcast_right, 7, 11, 53)
		map_sword = textures_to_map(walk_back_sword, walk_front_sword, walk_left_sword, walk_right_sword, 9, 9, 55)
		map_sword2 = textures_to_map(slash_back_sword, slash_front_sword, slash_left_sword, slash_right_sword, 6, 5, 55)
		map_crossbow = textures_to_map(walk_back_crossbow, walk_front_crossbow, walk_left_crossbow, walk_right_crossbow, 9, 9, 55)
		map_crossbow2 = textures_to_map(shoot_back_crossbow, shoot_front_crossbow, shoot_left_crossbow, shoot_right_crossbow, 6, 5, 55)
		map_goblin = textures_to_map(goblin_walk_back, goblin_walk_front, goblin_walk_left, goblin_walk_right, 6, 2, 57)
		map_goblin2 = textures_to_map(goblin_attack_back, goblin_attack_front, goblin_attack_left, goblin_attack_right, 5, 0, 65)
		map_golem = textures_to_map(golem_walk_back, golem_walk_front, golem_walk_left, golem_walk_right, 7, 0, 64)
		map_golem2 = textures_to_map(golem_attack_back, golem_attack_front, golem_attack_left, golem_attack_right, 7, 1, 76)
	end

	fun textures_to_map(t1, t2, t3, t4: Texture, nb_frames, pixel_top, height: Int): HashMap[Orientation, Array[Texture]]
	do
		var a1 = [for i in [0..nb_frames-1] do t1.subtexture(i*64, pixel_top, 64, height)]
		var a2 = [for i in [0..nb_frames-1] do t2.subtexture(i*64, pixel_top, 64, height)]
		var a3 = [for i in [0..nb_frames-1] do t3.subtexture(i*64, pixel_top, 64, height)]
		var a4 = [for i in [0..nb_frames-1] do t4.subtexture(i*64, pixel_top, 64, height)]
		var map = new HashMap[Orientation, Array[Texture]]
		map[ori_back] = a1
		map[ori_front] = a2
		map[ori_left] = a3
		map[ori_right] = a4
		return map
	end
end

redef class TileSet
	var spacing: Numeric = 1.0

	redef fun subtextures
	do
		var subtextures = new Array[Texture]
		for j in [0..nb_rows[do
			for i in [0..nb_cols[do
				subtextures.add texture.subtexture(
					i.to_f*width.to_f, j.to_f*height.to_f, width.to_f-spacing.to_f, height.to_f-spacing.to_f)
			end
		end
		return subtextures
	end
end

# 4 way orientation, `ori_left`, `ori_right`, `ori_front` and `ori_back`
class Orientation
	private var i: Int
	redef fun hash do return i
	redef fun ==(o) do return o isa Orientation and o.i == i
end

redef class Sys
	# Left orientation
	var ori_left = new Orientation(0)

	# Right orientation
	var ori_right = new Orientation(1)

	# Front orientation
	var ori_front = new Orientation(2)

	# Back orientation
	var ori_back = new Orientation(3)
end
