import ::android
import ::android::vibration
import ::android::landscape

import dawn_arrival
import touch_ui

redef class Enemy
	redef fun hurt(x)
	do
		super
		#app.vibrator.vibrate 5
	end

	redef fun die
	do
		super
		#app.vibrator.vibrate 10
	end
end

redef class Player
	redef fun die
	do
		super
		#app.vibrator.vibrate 20
	end

	redef fun pickup(w)
	do
		super
		#app.vibrator.vibrate 10
	end
end
