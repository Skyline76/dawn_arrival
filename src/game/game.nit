import levels
import entities
import noise

redef class LevelInfo
	fun load(drawing: DrawingImages): Level
	do
		var lvl = new Level(self)

		var data = asset.load
		var error = asset.error
		if error != null then
			print_error "Error loading at '{asset.path}: {error}"
			return lvl
		end

		var pattern_enemy = 0
		var start_t: nullable Tile = null
		var end_t: nullable Tile = null
		var spawn_tiles = new Array[Tile]

		# Setting depending on the selected generator
		var generator: WorldGenerator
		var grasses = drawing.grasses
		var prop = drawing.green_bush

		# The first line should declare the generator to use
		var lines = data.split('\n')
		var first_line = lines.shift
		var generator_name = first_line.trim
		if generator_name == "dungeon" then
			generator = new BSPGenerator
			grasses = [drawing.dungeon_floor]
			prop = drawing.dungeon_pillar
		else if generator_name == "overworld" then
			generator = new NoiseOverworldGenerator(false)
		else if generator_name == "island" then
			generator = new NoiseOverworldGenerator(true)
		else
			print_error "Unknown world generator {generator_name}"
			abort
		end

		# Pre compute the size of the static level
		var width = 0
		for line in lines do width = width.max(line.trim.length)
		var height = lines.length

		# Build tiles and level
		var x = -width/2
		var y = height/2
		for line in lines do
			for c in line.chars do
				var tile
				var pos = new Point3d[Float](x.to_f, y.to_f, 0.0)
				if c == 'B' then
					tile = new PropTile(pos, lvl, prop)
				else if c == 'W' then
					tile = new WaterTile(pos, lvl, drawing.water_grass_up)
				else if c == '.' then
					tile = new GrassTile(pos, lvl, grasses.rand)
				else if c == 'D' then
					tile = new DirtTile(pos, lvl, drawing.dirt)
				else if c == 'F' then
					tile = new DecorTile(pos, lvl,  drawing.flowers.rand)
				else if c == '$' then
					tile = new DirtTile(pos, lvl, drawing.dirt)
					var sword = new Weapon.from_tile(lvl, drawing.sword_shiny, 20, false, tile, drawing.map_sword, drawing.map_sword2, 6)
				else if c == 'C' then
					tile = new DirtTile(pos, lvl, drawing.dirt)
					var crossbow = new Weapon.from_tile(lvl, drawing.crossbow, 20, true, tile, drawing.map_crossbow, drawing.map_crossbow2, 6)
				else if c != ' ' then
					tile = new GrassTile(pos, lvl, grasses.rand)
					if c == 'S' then
						starting_position.x = x.to_f
						starting_position.y = y.to_f
					else if c == '!' then
						pattern_enemy+=1
						if pattern_enemy == 1 then
							start_t = tile
						else if pattern_enemy == 2 then
							end_t = tile
							if start_t != null then var pattern = new PatternEnemy(new Point3d[Float](start_t.center.x.to_f, start_t.center.y.to_f, 0.2), lvl, drawing.map_goblin[ori_back][0], 40, ori_back, drawing.goblin_dead, drawing.map_goblin, drawing.map_goblin2, 10, start_t, end_t)
							pattern_enemy = 0
						end
					else if c == '%' then
						var guard = new GuardEnemy(new Point3d[Float](tile.center.x.to_f, tile.center.y.to_f, 0.2), lvl, drawing.map_golem[ori_back][0], 80, ori_back, drawing.golem_dead, drawing.map_golem, drawing.map_golem2, 10, tile)
					end
				end
				x+=1
			end
			y -= 1
			x = -width/2
		end

		# Procedural world generation
		generator.generate_map(lvl, spawn_tiles, drawing)

		# Morph dirt and water tile appearance
		for l in [-60 .. 60] do
			for m in [-60 .. 60] do
				var tile = lvl.grid[l,m]
				if tile != null and (tile.is_dirt or tile.is_water) then
					morph(tile, drawing)
				end
			end
		end

		# Generate monsters
		for i in [0 .. 4] do
			if spawn_tiles.is_empty then break
			var tile = spawn_tiles.rand
			var guard = new GuardEnemy(new Point3d[Float](tile.center.x.to_f, tile.center.y.to_f, 0.2), lvl, drawing.map_golem[ori_back][0], 80, ori_back, drawing.golem_dead, drawing.map_golem, drawing.map_golem2, 10, tile)
			spawn_tiles.remove tile
		end

		for i in [0 .. 4] do
			if spawn_tiles.is_empty then break
			var tile = spawn_tiles.rand
			pattern_enemy+=1
			if pattern_enemy == 1 then
				start_t = tile
			else if pattern_enemy == 2 then
				end_t = tile
				if start_t != null then var pattern = new PatternEnemy(new Point3d[Float](start_t.center.x.to_f, start_t.center.y.to_f, 0.2), lvl, drawing.map_goblin[ori_back][0], 40, ori_back, drawing.goblin_dead, drawing.map_goblin, drawing.map_goblin2, 10, start_t, end_t)
				pattern_enemy = 0
			end
			spawn_tiles.remove tile
		end

		return lvl
	end

	fun morph(tile: Tile, drawing: DrawingImages)
	do
		if tile.is_dirt then
			var level = tile.level
			var x = tile.center.x.to_i
			var y = tile.center.y.to_i
			if level.get_tile(x-1, y).is_dirt then
				if level.get_tile(x+1, y).is_dirt then
					if level.get_tile(x, y-1).is_dirt then
						if level.get_tile(x, y+1).is_dirt then
							tile.update_texture(drawing.dirt1)
						else
							tile.update_texture(drawing.dirt_cross_down)
						end
					else if level.get_tile(x, y+1).is_dirt then
						tile.update_texture(drawing.dirt_cross_up)
					else
						tile.update_texture(drawing.dirt_horizontal)
					end
				else if level.get_tile(x, y-1).is_dirt or level.get_tile(x, y+1).is_dirt then
					tile.update_texture drawing.dirt_cross_left
				else
					tile.update_texture drawing.dirt_end_right
				end
			else if level.get_tile(x+1, y).is_dirt then
				if level.get_tile(x, y-1).is_dirt or level.get_tile(x, y+1).is_dirt then
					tile.update_texture drawing.dirt_cross_right
				else
					tile.update_texture drawing.dirt_end_left
				end
			else if level.get_tile(x, y-1).is_dirt then
				if level.get_tile(x, y+1).is_dirt then
					tile.update_texture drawing.dirt_vertical
				else
					tile.update_texture drawing.dirt_end_up
				end
			else if level.get_tile(x, y+1).is_dirt then
				tile.update_texture drawing.dirt_end_down
			else
				tile.update_texture drawing.dirt
			end

			tile.update_sprite
		end

		if tile.is_water then
			var level = tile.level
			var x = tile.center.x.to_i
			var y = tile.center.y.to_i
			var water = false
			if level.get_tile(x-1, y).is_water then
				if level.get_tile(x+1, y).is_water then
					if level.get_tile(x, y-1).is_water then
						if level.get_tile(x, y+1).is_water then
							if water then
								tile.update_texture(drawing.water1)
								water = false
							else
								tile.update_texture(drawing.water2)
								water = true
							end
						else
							tile.update_texture(drawing.water_grass_up)
						end
					else if level.get_tile(x, y+1).is_water then
						tile.update_texture(drawing.water_grass_bottom)
					end
				else if level.get_tile(x, y-1).is_water then
					if level.get_tile(x, y+1).is_water then
						tile.update_texture drawing.water_grass_right
					else
						tile.update_texture drawing.water_grass_up_right
					end
				else if level.get_tile(x, y+1).is_water then
					tile.update_texture drawing.water_grass_bottom_right
				else
					tile.update_texture drawing.water_grass_right
				end
			else if level.get_tile(x+1, y).is_water then
				if level.get_tile(x, y-1).is_water then
					if level.get_tile(x, y+1).is_water then
						tile.update_texture drawing.water_grass_left
					else
						tile.update_texture drawing.water_grass_up_left
					end
				else if level.get_tile(x, y+1).is_water then
					tile.update_texture drawing.water_grass_bottom_left
				else
					tile.update_texture drawing.water_grass_left
				end
			else if level.get_tile(x, y-1).is_water then
				if not level.get_tile(x, y+1).is_water then
					tile.update_texture drawing.water_grass_up
				end
			else if level.get_tile(x, y+1).is_water then
				tile.update_texture drawing.water_grass_bottom
			end

			tile.update_sprite
		end
	end
end

class Game
	private var lvl: Level = new Level(null)

	var player: Player

	var level: Level

	var fists: Weapon

	init(level: Level, player: Player, drawing: DrawingImages)
	do
		fists = new Weapon.from_tile(level, drawing.dirt, 1, false, new Tile(new Point3d[Float](600.0, -600.0, -4.0), level, drawing.dirt), drawing.map_fists, drawing.map_fists2, 6)
		self.level = level
		self.player = player
	end
end

# Map generation service
interface WorldGenerator

	# Actual map generation algorithm
	fun generate_map(lvl: Level, spawn_tiles: Array[Tile], drawing: DrawingImages) is abstract
end

# Map generator based on `InterpolatedNoise`
class NoiseOverworldGenerator
	super WorldGenerator

	# Is the map surrounded by water and contain the goal treasure?
	var island: Bool

	redef fun generate_map(lvl, spawn_tiles, drawing)
	do
		var map = new IslandNoise
		map.n_layers = 3
		map.period = 70.0
		map.island = island

		# Position of possible gates to the next level
		var possible_gates = new Array[Point3d[Float]]

		for j in [-60 .. 60] do
			for k in [-60 .. 60] do
				var tile = lvl.get_tile(j,k)
				if tile.is_void then
					var perlin = map[j.to_f,k.to_f]
					var pos = new Point3d[Float](j.to_f, k.to_f, 0.0)
					if perlin < 0.42 then
						tile = new PropTile(pos, lvl, drawing.green_bush)
					else if perlin < 0.47 then
						tile = new GrassTile(pos, lvl, drawing.grasses.rand)
						spawn_tiles.add tile
						possible_gates.add pos
					else if perlin < 0.51 then
						tile = new DirtTile(pos, lvl, drawing.dirt1)
						spawn_tiles.add tile
					else if perlin < 0.53 then
						tile = new DecorTile(pos, lvl, drawing.flowers.rand)
						spawn_tiles.add tile
					else
						tile = new WaterTile(pos, lvl, drawing.water1)
					end
				end
			end
		end

		if island then
			# Place a few end goal treasures
			var n_treasures = 2
			for i in [0..n_treasures] do
				var gate_pos = possible_gates.rand
				var gate_tile = new GateTile(gate_pos, lvl, drawing.treasure, "forest")
				# TODO trigger end game credits or something
			end
		else
			# Place a few random gates
			# Having more than one gate makes it more probably that at least one is accessible.
			var n_gates = 4
			for i in [0..n_gates] do
				var gate_pos = possible_gates.rand
				var gate_tile = new GateTile(gate_pos, lvl, drawing.dungeon_stairs, "dungeon")
			end
		end
	end
end

# Modified noise to produce an island surrounded by water or trees
class IslandNoise
	super PerlinNoise

	# Should the island be surrounded by water? Otherwise it's trees
	var island = true

	redef fun [](x, y)
	do
		var v = super

		# Compute p, at 0.0 in the center, increasing up to 1.0 at 60.0
		# from the center, and above farther away
		var max_radius = 60.0
		var radius = (x*x + y*y).sqrt
		var p = radius / max_radius
		p = p.pow(16.0) * 0.5
		#print "({x},{y}) {p}"

		# + to shift towards water, - to shift towards trees
		if not island then p = -p

		return v + p
	end
end

# Dungeon generator using binary partitioning
#
# See the following wiki for a graphical explanation of the BSP algorithm:
# http://www.roguebasin.com/index.php?title=Basic_BSP_Dungeon_generation
#
# The result may look like the following:
# ~~~nitish
# ------------------------------
# --##-######--######----#####--
# --##-######--###############--
# --##-######--######-#--#####--
# --#########--######-#--#####--
# --##-######---------#---------
# --##-######----######---------
# --##--#--------######---####--
# ------#--------######---####--
# ------###############---####--
# --#####--------######---####--
# --#####--###---#############--
# --#####--###---######---####--
# --##########---######---####--
# --#####--###---######---####--
# --#####--###---######---####--
# ---------------######---####--
# ---------------######---------
# ------------------------------
# ~~~
class BSPGenerator
	super WorldGenerator

	redef fun generate_map(lvl, spawn_tiles, drawing)
	do
		var w = 40
		var h = 40

		# Build rooms as a double map of booleans
		var tree = new BSPTree(4) # Use a higher value for more smaller rooms
		var tiles = new HashMap2[Int, Int, Bool]
		tree.build_dungeon(tiles, -w+1, -h+1, 2*w-2, 2*h-2)

		# Debug prints
		#for y in h.times do
			#for x in w.times do
				#printn if tiles[x, y] or else false then "#" else "-"
			#end
			#print ""
		#end

		var possible_gates = new Array[Point3d[Float]]

		# Build the level tiles
		for y in [-h..h] do
			for x in [-w..w] do
				var tile = lvl.get_tile(x, y)
				if tile.is_void then
					var pos = new Point3d[Float](x.to_f, y.to_f, 0.0)

					# Floor?
					if tiles[x, y] == true then
						tile = new GrassTile(pos, lvl, drawing.dungeon_floor)
						spawn_tiles.add tile
						continue
					end

					# Wall?
					for xi in [-1..1] do
						for yi in [-1..1] do
							var next_tile = lvl.grid[x+xi, y+yi]
							if tiles[x+xi, y+yi] == true or next_tile isa GrassTile then
								tile = new PropTile(pos, lvl, drawing.dungeon_wall)

								# Is under a floor tile?
								if tiles[x, y-1] == true then possible_gates.add pos

								break label
							end
						end
					end label
				end
			end
		end

		# Add a gate at the top of a random room
		var n_gates = 4
		for i in [0..n_gates[ do
			var gate_pos = possible_gates.rand
			var gate_tile = new GateTile(gate_pos, lvl, drawing.dungeon_gate, "island")
		end
	end
end

# Binary space partitioning tree
#
# ~~~nitish
# var w = 60
# var h = 60
# var tree = new BSPTree(3) # Use a higher value for more smaller rooms
# var tiles = new HashMap2[Int, Int, Bool]
# tree.build_dungeon(tiles, 1, 1, w-2, h-2)
# ~~~
private class BSPTree
	# Dist to leaves
	var depth: Int

	# Physical partition
	var p: Float = 0.25 + 0.5.rand

	var children = new Array[BSPTree]

	init
	do
		if depth > 0 then
			children.add new BSPTree(depth-1)
			children.add new BSPTree(depth-1)
		end
	end

	fun build_dungeon(tiles: HashMap2[Int, Int, Bool], left, top, width, height: Int)
	do
		if depth > 0 then
			var horizontal = width > height
			if horizontal then
				# Build rooms in leaves
				var pi = (width.to_f*p).to_i
				children[0].build_dungeon(tiles, left, top, pi, height)
				children[1].build_dungeon(tiles, left+pi, top, width-pi, height)

				# Build corridors between rooms
				var y = top + height / 2
				for x in [left+pi/2 .. left+(pi+width)/2] do
					if tiles[x, y ] == null then tiles[x, y] = true
					if tiles[x, y+1] == null then tiles[x, y+1] = true
				end
			else
				# Build rooms in leaves
				var pi = (height.to_f*p).to_i
				children[0].build_dungeon(tiles, left, top, width, pi)
				children[1].build_dungeon(tiles, left, top+pi, width, height-pi)

				# Build corridors between rooms
				var x = left + width / 2
				for y in [top+pi/2 .. top+(pi+height)/2] do
					if tiles[x,   y] == null then tiles[x, y] = true
					if tiles[x+1, y] == null then tiles[x+1, y] = true
				end
			end
			return
		end

		# Leaf
		var l = left + (width/3).rand + 1
		var t = top + (height/3).rand + 1
		var r = left + width - (width/3).rand - 1
		var b = top + height - (height/3).rand - 1
		for y in [t..b] do
			for x in [l..r] do
				tiles[x, y] = true
			end
		end
	end
end
