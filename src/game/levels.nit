import geometry
import more_collections
import app
import gamnit::textures
import drawing

class Grid
	super HashMap2[Int, Int, Tile]

	redef fun []=(x, y, v)
	do
		var t = self[x, y]
		if t != null then t.destroy
		super
	end
end

class LevelInfo
	var name: String

	var asset = new TextAsset("levels" / name + ".txt") is lazy

	public var starting_position = new Point3d[Float](0.0, 0.0, 0.0)
end

class Level
	var grid = new Grid

	var info: nullable LevelInfo

	fun get_tile(x, y: Int): Tile
	do
		var t = grid[x, y]
		if t != null then return t
		t = new VoidTile(x, y, self)
		grid[x,y] = t
		return t
	end

	fun is_weapon(x, y: Int): nullable Weapon
	do
		var res: nullable Weapon = null
		for w in weapons do
			if w.spawn_tile == get_tile(x, y) then
				res = w
			end
		end
		return res
	end

	var weapons = new Array[Weapon]

	var entities = new Set[Entity]
end

class Entity
	var center: Point3d[Float]

	var level: Level

	var texture: Texture

	fun destroy do level.entities.remove self

	var sprite = new Sprite(texture, center) is lazy

	init
	do
		sprite.scale = 0.063
		level.entities.add self
	end

	fun update_texture(t: Texture)
	do
		texture = t
	end

	fun update_sprite
	do
		sprite.texture = texture
		sprite.center = center
	end
end

class Tile
	super Entity

	var is_collidable: Bool = false
	var is_void: Bool = false
	var is_dirt: Bool = false
	var is_water: Bool = false

	#var hitbox: Box[Float]

	init
	do
		level.entities.add self
		level.grid[center.x.to_i, center.y.to_i]=self
	end

	fun damage: Int do return 0
end

class CollidableTile
	super Tile
end

class TrapTile
	super Tile

	var dmg: Int

	redef fun damage do return dmg
end

# Gate to a different level
class GateTile
	super Tile

	# Name of the level descriptor in the assets directory
	var level_name: String
end

class DecorTile
	super Tile
end

class GrassTile
	super Tile
end

class DirtTile
	super Tile
	init do
		super
		is_dirt = true
	end
end

class VoidTile
	super Tile
	init(x, y: Int, lvl: Level) do
		center = new Point3d[Float](x.to_f, y.to_f, 0.0)
		texture = once new Texture("level/void.png")
		is_void = true
		super(center, lvl, texture)
	end
end

class WaterTile
	super CollidableTile
	init do
		super
		is_water = true
	end
end

class PropTile
	super CollidableTile
end

class ActiveEntity
	super Entity

	var life: Int
	fun is_alive: Bool do return life > 0

	var position: Orientation

	var death_anim: Array[Texture]
	var cpt_death: Int = 0

	fun respawn(x: Int)
	do
		life = x
		cpt_death = 0
	end

	fun move_to(p: Point3d[Float])
	do
		center = p
	end
	#var hitbox: nullable Box[Float] is noinit
end

class Weapon
	super Entity

	var walk_anim: HashMap[Orientation, Array[Texture]]
	var attack_anim: HashMap[Orientation, Array[Texture]]
	var nb_anim_attack: Int

	var dmg: Int
	#Is this a range weapon or not?
	var range: Bool
	var spawn_tile: Tile
end

class Projectile
	super Entity

	var orientation: Orientation
end
