# This file is part of NIT ( http://www.nitlanguage.org ).
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the Do What The Fuck You Want To
# Public License, Version 2, as published by Sam Hocevar. See
# http://sam.zoy.org/projects/COPYING.WTFPL for more details.

# Hack'n'slash game for the gamnit framework
module dawn_arrival is
	app_name "Dawn Arrival"
	app_version(1, 0, git_revision)
end

import gamnit::flat # For `Texture, Sprite`, etc.
import gamnit::keys # For `pressed_keys`
import gamnit::tileset
import app::audio

import drawing
import game

redef class App

	var assets = new DrawingImages

	private var cpt: Int
	private var change_sprite = false

	var clock = new Clock
	var spawn_texture: Texture = assets.map_fists[ori_back][0]
	var night_maroon = new Sprite(assets.night_maroon, new Point3d[Float](0.0, 0.0, -1.0))
	var night_blue = new Sprite(assets.night_blue, new Point3d[Float](0.0, 0.0, -1.0))
	var hearts = new Array[Sprite]
	var daytime = 0.0
	var game: nullable Game = null

	# Pretty font for `tutorial_text`
	var font = new BMFontAsset("grand_hotel/font.fnt")

	# Tutorial text field
	var tutorial_text = new TextSprites(font, ui_camera.bottom.offset(0.0, 64.0, 0.0), align=0.5, valign=1.0, max_width=640.0) is lazy

	private var perf_clock = new Clock
	private var perf_print_clock = new Clock
	private var perf_print_cooldown = 0.0

	redef fun on_create
	do
		perf_clock.lapse
		super
		maximum_fps = 60.0
		perfs["on_create load"].add perf_clock.lapse


		var splash = new Texture("loading.png")
		show_splash_screen splash

		# Report errors on all loaded textures.
		# Root textures are associated to pixel data,
		# whereas other texture may be subtextures of root textures.
		for tex in all_root_textures do
			var error = tex.error
			if error != null then print_error "Texture '{tex}' failed to load: {error}"
			tex.as(RootTexture).pixelated = true
		end

		# Move the camera to show 10 world units on the Y axis at Z = 0.
		# The `sprite` should take approximately 1/4 of the height of the screen.
		world_camera.reset_height 15.0

		# Move the near clip wall closer to the camera because our world unit
		# range is small. Moving the clip wall too close to the camera can
		# cause glitches on mobiles devices with small depth buffer.
		world_camera.near = 1.0

		# Make the background blue and opaque.
		#glClearColor(0.0, 1.0, 0.0, 1.0)
		perfs["on_create misc"].add perf_clock.lapse

		# Select a level at random
		#
		# TODO don't select one at random, start in the overworld and move to
		# the dungeons after the first gate.
		prepare_game("forest", null)
		perfs["on_create prepare_game"].add perf_clock.lapse

		night_maroon.scale = 100.0
		night_blue.scale = 100.0
		night_maroon.alpha = 0.0
		night_blue.alpha = 0.0
		ui_sprites.add night_maroon
		ui_sprites.add night_blue
		for i in [0..4] do
			hearts[i] = new Sprite(assets.full_heart, ui_camera.top_left.offset(23.0 + 39.0 * i.to_f, -20.0, 5.0))
			ui_sprites.add hearts[i]
		end

		var m = new Music("Soundtrack.mp3")
		m.play
	end

	private var prepare_game_clock = new Clock is lazy

	# Clean up current game (if any) and load the level with `name`
	fun prepare_game(name: String, weapon: nullable Weapon): Game
	do
		prepare_game_clock.lapse
		# Clean up old game
		sprites.clear

		# Load new game
		var info = new LevelInfo(name)
		var level = info.load(assets)
		var fists = new Weapon.from_tile(level, assets.dirt, 1, false, new Tile(new Point3d[Float](600.0, -600.0, -4.0), level, assets.dirt), assets.map_fists, assets.map_fists2, 6)
		var player = new Player(info.starting_position.offset(0.0, 0.0, 0.1), level, spawn_texture, 1000, ori_back, assets.dead, fists)
		var game = new Game(level, player, assets)

		world_camera.position.x = game.level.info.starting_position.x
		world_camera.position.y = game.level.info.starting_position.y

		for e in game.level.entities do sprites.add e.sprite
		perfs["prepare_game"].add prepare_game_clock.lapse

		# Print the goal
		if self.game == null then
			# Start, 1st level
			tutorial_text.text = "Reach the island through the dungeon to find the treasure!\nLook for stairs to the dungeon..."
		else if name == "dungeon" then
			# 2nd level
			tutorial_text.text = "Find a door leading out of the dungeon to reach the island!"
		else if name == "island" then
			# 3rd level
			tutorial_text.text = "You're almost there, search the island to find the treasure!"
		else if name == "forest" then
			# Won
			tutorial_text.text = "Congratulations, you won!\nYou can find more treasure by going to other islands."
		end

		self.game = game
		return game
	end

	redef fun update(dt)
	do
		perf_clock.lapse

		var projectiles_to_remove = new Array[Entity]

		# Update game logic here.
		var game = game
		if game == null then return
		var lvl = game.level
		var player = game.player

		var w = lvl.is_weapon(player.center.x.round.to_i, player.center.y.round.to_i)
		if w != null then
			player.pickup(w)
			w.update_texture(assets.void)
			w.update_sprite
		end

		# Move `sprite` with the keyboard arrows.
		# Set the speed according to the elapsed time since the last frame `dt`
		# for a smooth animation.
		if player.is_alive then
			if not player.attacking then
				var dx = 0.0
				var dy = 0.0
				for key in pressed_keys do
					if key == "left" then
						dx -= 1.0
					else if key == "right" then
						dx += 1.0
					else if key == "up" then
						dy += 1.0
					else if key == "down" then
						dy -= 1.0
					else if key == "e" then
						player.hurt(1)
					else if key == "space" then
						if player.weapon.range then
							player.attack
						end
					end

					var camera_margin = 0.0
					var camera_margin_y = 0.0
					var cx = world_camera.position.x - player.center.x
					var cy = world_camera.position.y - player.center.y
					if cx.abs > camera_margin then world_camera.position.x -= cx - (cx/cx.abs)*camera_margin
					if cy.abs > camera_margin_y then world_camera.position.y -= cy - (cy/cy.abs)*camera_margin_y
				end

				# Limit top speed at 1.0 when moving in diagonals
				if dx != 0.0 or dy != 0.0 then
					var s = (dx*dx + dy*dy).sqrt
					dx = dx/s
					dy = dy/s
				end

				if dx != 0.0 or dy != 0.0 then
					player.move(dt, dx, dy)
				end
			else
				player.attack
			end
			if player.life < 900 then hearts[4].texture = assets.half_heart
			if player.life < 800 then hearts[4].texture = assets.empty_heart
			if player.life < 700 then hearts[3].texture = assets.half_heart
			if player.life < 600 then hearts[3].texture = assets.empty_heart
			if player.life < 500 then hearts[2].texture = assets.half_heart
			if player.life < 400 then hearts[2].texture = assets.empty_heart
			if player.life < 300 then hearts[1].texture = assets.half_heart
			if player.life < 200 then hearts[1].texture = assets.empty_heart
			if player.life < 100 then hearts[0].texture = assets.half_heart
		else
			player.die
			hearts[0].texture = assets.empty_heart
			for key in pressed_keys do
				if key == "space" then
					player.respawn(1000)
					player.update_texture(spawn_texture)
					player.move_to game.level.info.starting_position.offset(0.0, 0.0, 0.1)
					player.update_sprite
					for i in [0..4] do
						ui_sprites.remove hearts[i]
						hearts[i] = new Sprite(assets.full_heart, ui_camera.top_left.offset(23.0 + 39.0 * i.to_f, -20.0, 5.0))
						ui_sprites.add hearts[i]
					end
				end
			end
		end
		if player.projectiles.length != 0 then
			for p in player.projectiles do
				if p.orientation == ori_right then do p.sprite.center.x +=0.5
				if p.orientation == ori_left then do p.sprite.center.x -=0.5
				if p.orientation == ori_front then do p.sprite.center.y -=0.5
				if p.orientation == ori_back then do p.sprite.center.y +=0.5

				for e in lvl.enemies do
					var dy = (p.sprite.center.y - e.sprite.center.y).abs + 0.5
					var dx = (p.sprite.center.x - e.sprite.center.x).abs + 0.5

					var collisionY = dy <= assets.arrow.height/2.0*p.sprite.scale + e.sprite.texture.height/2.0*e.sprite.scale
					var collisionX = dx <= assets.arrow.width/2.0*p.sprite.scale + e.sprite.texture.width/2.0*e.sprite.scale

					if collisionY and collisionX then
						e.hurt(player.weapon.dmg)
						if not projectiles_to_remove.has(p) then
							projectiles_to_remove.add p
						end
					end
				end
			end
		end

		for s in projectiles_to_remove do
			sprites.remove s.sprite
			player.projectiles.remove s
		end

		projectiles_to_remove.clear


		if player.delay_fire > 0.0 then
			player.delay_fire -= dt
		end

		perfs["update player"].add perf_clock.lapse

		for e in lvl.enemies do
			if not player.is_alive then e.disengage
			if not e.is_alive then
				e.die
				e.tick_respawn dt
				if e.cpt_respawn >= 10.0 then app.sprites.remove_all e.sprite
				if e.cpt_respawn >= 20.0 then
					e.respawn 40
					sprites.add e.sprite
				end
			else
				e.engage
				e.tick(dt)
			end
		end
		perfs["update enemies"].add perf_clock.lapse

		# Day/night cycle
		if daytime > 90.0 and daytime < 110.0 then
			night_maroon.alpha += dt/50.0
		else if daytime <= 90.0 then
			if night_maroon.alpha > 0.0 then night_maroon.alpha -= dt/35.0
		else
			if daytime > 200.0 then
				if night_maroon.alpha < 0.4 then night_maroon.alpha += dt/50.0
				if night_blue.alpha > 0.0 then night_blue.alpha -= dt/40.0
				if daytime > 220.0 then daytime = 0.0
			else
				if night_maroon.alpha > 0.0 then night_maroon.alpha -= dt/40.0
				if night_blue.alpha < 0.4 then night_blue.alpha += dt/50.0
			end
		end
		daytime += dt
		perfs["update day/night cycle"].add perf_clock.lapse

		# Print perf stats every 5 seconds
		perf_print_cooldown += perf_print_clock.lapse
		if perf_print_cooldown > 5.0 then
			print "{current_fps} fps"
			print sys.perfs
			perf_print_cooldown = 0.0
		end
	end

	redef fun accept_event(event)
	do
		if super then return true

		if event isa QuitEvent or
		  (event isa KeyEvent and event.name == "escape" and event.is_up) then
			# When window close button, escape or back key is pressed
			# show the average FPS over the last few seconds.
			print "{current_fps} fps"
			print sys.perfs

			# Quit abruptly
			exit 0
		end
		return false
	end

	fun add_to_sprites(arr: Array[Sprite], sprite: Sprite)
	do
		for s in arr do
			if s.center == sprite.center then arr.remove s
		end
		arr.add sprite
	end
end

redef class Player
	redef fun move(dt, dx, dy)
	do
		super

		if dx < 0.0 then
			texture = weapon.walk_anim[position][cpt%8]
			self.update_sprite
			if change_sprite then
				cpt += 1
				change_sprite = false
			else
				change_sprite = true
			end
		else if dx > 0.0 then
			texture = weapon.walk_anim[position][cpt%8]
			self.update_sprite
			if change_sprite then
				cpt += 1
				change_sprite = false
			else
				change_sprite = true
			end
		else if dy < 0.0 then
			texture = weapon.walk_anim[position][cpt%8]
			self.update_sprite
			if change_sprite then
				cpt += 1
				change_sprite = false
			else
				change_sprite = true
			end
		else if dy > 0.0 then
			texture = weapon.walk_anim[position][cpt%8]
			self.update_sprite
			if change_sprite then
				cpt += 1
				change_sprite = false
			else
				change_sprite = true
			end
		end

		# Is it a gate?
		var tile = level.get_tile(center.x.round.to_i, (bottom-dist_to_feet).round.to_i)
		if tile isa GateTile then
			app.prepare_game tile.level_name
		end
	end

	var delay_fire = 0.0
	var fire_rate = 0.5

	redef fun attack
	do
		super

		if weapon.range then
			if delay_fire <= 0.0 then
				var projectile = new Projectile(new Point3d[Float](center.x, center.y, 0.1), level, app.assets.arrow, position)
				projectile.sprite.scale = 0.033
				if position == ori_right then do projectile.sprite.rotation = pi*1.5 # right
				if position == ori_left then do projectile.sprite.rotation = pi/2.0 # left
				if position == ori_front then do projectile.sprite.rotation = pi # down
				projectiles.add projectile
				app.sprites.add projectile.sprite
				delay_fire = fire_rate
			end
		end
	end
end
